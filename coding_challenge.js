const items = [
  { id: 2, seqId: 4, parent: 5, name: "index.tsx" },
  { id: 3, seqId: 3, parent: 1, name: "Sidebar" },
  { id: 4, seqId: 5, parent: 1, name: "Table" },
  { id: 7, seqId: 5, parent: 5, name: "SelectableDropdown.tsx" },
  { id: 5, seqId: 2, parent: 1, name: "AssignmentTable" },
  { id: 1, seqId: 1, parent: null, name: "components" },
  { id: 6, seqId: 2, parent: null, name: "controllers" }
];

const finalItems = transformItems(items);

/*
Create a function `transformItems` that would return the desired output below
(should be able to support virtually unlimited depth and additional items)
*/

function transformItems(items) {
    let parents = [];
    items.sort(function(a, b){return a.seqId - b.seqId});

    // Add parents and remove them in items list
    items.map((item, key) => {
        if (item.parent === null) {
            item = {...item, depth: 0}
            parents.push(item);
            items.splice(key, 1);
        }
    });

    // store remaining items
    let remainingItems = items;

    while(remainingItems.length) {
        let newParent = [];
        let children = [];
        let siblings = [];

        // loop parents
        parents.map((parent) => {
            siblings = [...remainingItems.filter(item=> item.parent === parent.id)];
            children = [...children, ...siblings];
            siblings.sort(function(a, b){return a.seqId - b.seqId});
            newParent = [...newParent, parent]
            if (siblings.length > 0) {
                newParent = [...newParent, ...siblings.map((sibling) => {return {...sibling, depth: parent.depth+1}})];
            }
        });
        remainingItems = remainingItems.filter(item => !children.includes(item));
        parents = newParent;
    }

    return parents;
}

console.log('Final Items: ', finalItems);

/* Output:
// The seqId is used for ordering within siblings.
// The depth would depend on the number of ancestors.
[
  { id: 1, seqId: 1, parent: null, depth: 0, name: 'components' },
  { id: 5, seqId: 2, parent: 1, depth: 1, name: 'AssignmentTable' },
  { id: 2, seqId: 4, parent: 5, depth: 2, name: 'index.tsx' },
  { id: 7, seqId: 5, parent: 5, depth: 2, name: 'SelectableDropdown.tsx' },
  { id: 3, seqId: 3, parent: 1, depth: 1, name: 'Sidebar' },
  { id: 4, seqId: 5, parent: 1, depth: 1, name: 'Table' },
  { id: 6, seqId: 2, parent: null, depth: 0, name: 'controllers' }
]
*/